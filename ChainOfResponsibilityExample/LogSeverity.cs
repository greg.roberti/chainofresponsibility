using System;

namespace ChainOfResponsibilityExample
{
    /// <summary>
    /// LogSeverity is a bitwise enum utilizing the [Flags] attribute which allows for combinations such as All.
    /// </summary>
    [Flags]
    public enum LogSeverity
    {
        None = 0,

        Emergency = 1,

        Alert = 2,

        Critical = 4,

        Error = 8,

        Warning = 16,

        Notice = 32,

        Info = 64,

        Debug = 128,

        All = 255,
    }
}