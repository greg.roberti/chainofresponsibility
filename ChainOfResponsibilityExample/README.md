# Chain of Responsibility
The Chain of Responsibility design pattern is part of the collection of design patterns documented by the "Gang of Four" (or GoF) in their now famous book, **Design Patterns:
Elements of Reusable Object-Oriented Software**.

This particular pattern is extremely useful because it gives multiple receiving objects an opportunity to handle and then pass along a request to the next handler. A flexible chain of handlers is established in this way providing a simplified interface to the user. Here we are able to create a decoupling between the originator of a request and any number of objects created to handle it.

The pattern is implemented by creating a sort of linked list, tacking one handler onto the end of a chain of handlers. Each handler is then derived from an abstract base class and extends the functionality of the base to provide its own concrete implementation.

## Logging Example:
In this simple example repository a series of loggers are chained together, each logging when and how it is appropriate for them according to the rules that govern their behavior.

In the **Main** class, the user simply invokes a single logger and provides the message they wish to log. There is no need to specify where and how each message should be logged since that behavior is established once when the logger is initialized. Convenience methods have also been created for each severity level such as **LogWarning()** to hide away the complexities associated with referencing the correct enum value every time and provide an extremely intuitive API for the end user.

While initializing the logger, the user decides where and how each of the logs will be processed based on the severity of the log. In this example, all logs are sent to the console via the **ConsoleLogger** and then different log files are also created based on log severity via the **FileLogger** which is initialized multiple times with different sets of parameters.

Upon invocation of the logger, each handler will take turns processing the request and will only carry out the action when the conditions defined during initialization have been met. Regardless of whether a given logger in the chain actually logged the message, it still calls the next logger.

After running this example code, examine the console and each of the log files created by the application to see where each of the messages ended up. You should see all logs appear in the console as well as the file **AllLogs.txt** since both of these handlers were created with **LogSeverity.All**, however, only a subset of the total messages will appear in each of the other log files. Take note of the file **AllErrors.txt** which should contain all messages logged with one of the following severities: **Error**, **Critical**, **Alert**, or **Emergency**.