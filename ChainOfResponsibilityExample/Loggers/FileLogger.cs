using System.IO;

namespace ChainOfResponsibilityExample.Loggers
{
    /// <summary>
    /// FileLogger will write a log message to the specified log file.
    /// In this example all log files will be hardcoded to the LOGS directory and are appended with the .txt extension.
    /// </summary>
    public class FileLogger : LoggerBase
    {
        private readonly string _logFile;

        public FileLogger(string filename, LogSeverity severityMask) : base(severityMask)
        {
            _logFile = Path.Combine("LOGS", filename + ".txt");
        }

        protected override void WriteMessage(string message)
        {
            using StreamWriter sw = File.AppendText(_logFile);
            sw.WriteLine(message);
        }
    }
}