namespace ChainOfResponsibilityExample.Loggers
{
    /// <summary>
    /// LoggerBase is the base class for all loggers implemented with the Chain of Responsibility pattern
    /// </summary>
    public abstract class LoggerBase
    {
        private readonly LogSeverity SeverityMask;

        private LoggerBase NextLogger;

        protected LoggerBase(LogSeverity severityMask)
        {
            SeverityMask = severityMask;
        }

        public LoggerBase SetNextLogger(LoggerBase nextLogger)
        {
            LoggerBase lastLogger = this;

            while (lastLogger.NextLogger != null)
            {
                lastLogger = lastLogger.NextLogger;
            }

            lastLogger.NextLogger = nextLogger;

            return this;
        }

        private void LogMessage(string message, LogSeverity logSeverity)
        {
            if ((logSeverity & SeverityMask) != 0)
            {
                WriteMessage(logSeverity.ToString().ToUpper() + ": " + message);
            }

            if (NextLogger != null)
            {
                NextLogger.LogMessage(message, logSeverity);
            }
        }

        protected abstract void WriteMessage(string message);

        public void LogDebug(string message)
        {
            LogMessage(message, LogSeverity.Debug);
        }

        public void LogInfo(string message)
        {
            LogMessage(message, LogSeverity.Info);
        }

        public void LogNotice(string message)
        {
            LogMessage(message, LogSeverity.Notice);
        }

        public void LogWarning(string message)
        {
            LogMessage(message, LogSeverity.Warning);
        }

        public void LogError(string message)
        {
            LogMessage(message, LogSeverity.Error);
        }

        public void LogCritical(string message)
        {
            LogMessage(message, LogSeverity.Critical);
        }

        public void LogAlert(string message)
        {
            LogMessage(message, LogSeverity.Alert);
        }

        public void LogEmergency(string message)
        {
            LogMessage(message, LogSeverity.Emergency);
        }

        public void LogAllSeverity(string message)
        {
            LogMessage(message, LogSeverity.All);
        }
    }
}