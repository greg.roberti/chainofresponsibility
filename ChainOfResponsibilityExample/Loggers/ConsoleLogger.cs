using System;

namespace ChainOfResponsibilityExample.Loggers
{
    /// <summary>
    /// ConsoleLogger will write a log message to the screen via the console.
    /// </summary>
    public class ConsoleLogger : LoggerBase
    {
        public ConsoleLogger(LogSeverity severityMask) : base(severityMask)
        {
        }

        protected override void WriteMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}