﻿using System;
using ChainOfResponsibilityExample.Loggers;

namespace ChainOfResponsibilityExample
{
    class Program
    {
        /// <summary>
        /// Initializes multiple loggers:
        /// - A new console logger is responsible for logging everything to the console.
        /// - Various file loggers are created for different levels of severity, each with its own log file.
        /// </summary>
        static LoggerBase InitializeLoggers()
        {
            return new ConsoleLogger(LogSeverity.All)
                .SetNextLogger(new FileLogger("AllLogs", LogSeverity.All))
                .SetNextLogger(new FileLogger("Debug", LogSeverity.Debug))
                .SetNextLogger(new FileLogger("Info", LogSeverity.Info))
                .SetNextLogger(new FileLogger("Operations", LogSeverity.Debug | LogSeverity.Info))
                .SetNextLogger(new FileLogger("WarningsOnly", LogSeverity.Warning))
                .SetNextLogger(new FileLogger("AllErrors", LogSeverity.Error | LogSeverity.Critical | LogSeverity.Alert | LogSeverity.Emergency))
                .SetNextLogger(new FileLogger("Emergency", LogSeverity.Emergency));
        }

        /// <summary>
        /// Example of logger in use. All errors are artificial. No animals were harmed in the making of this test.
        /// </summary>
        static void Main(string[] args)
        {
            LoggerBase logger = InitializeLoggers();

            logger.LogDebug("Logger Initialized Successfully at " + DateTime.UtcNow);

            logger.LogInfo("Application starting..");

            logger.LogNotice("Application startup delayed due to network issue.");

            logger.LogWarning("Switching to backup server due to ongoing network issues.");

            logger.LogError("Backup server failed to respond.");

            logger.LogCritical("Backup server has not responded within 30 seconds.");

            logger.LogAlert("ALL BACKUP SERVERS ARE NON-RESPONSIVE! SENDING PAGER DUTY ALERT!");

            logger.LogEmergency("ALL BACKUP SERVERS ARE NON-RESPONSIVE AND PAGER DUTY TEAM IS NOT RESPONDING!!!");

            logger.LogAllSeverity("SERVER IS NOW REBOOTING!");
        }
    }
}